from flask import Flask, request
import requests
import json
import numpy as np
import cv2 as cv2
# import pprint

root = 'https://api.telegram.org'
path = '/bot851968246:AAGT9gO713AUf9C0MJsRykyqz0_s_R1AlEY/sendMessage'
# path = '/bot667052156:AAFRQdHUTHVIQRc2s3lQNdh2PaZnVp38PyE/sendMessage'

app = Flask(__name__)

"""
basic nggo flask
https://www.tutorialspoint.com/flask/flask_url_building.htm
"""
session = {
    'chat_id' : [],
    'state' : [],
    'photos' : []}

# Define height and width for each cell in grid
height = 800
width = 800

def download_photo(url):
    # Download photo by url, then return the image in numpy
    req = requests.get(url)
    image_url = json.loads(req.text)
    image = requests.get('https://api.telegram.org/file/bot851968246:AAGT9gO713AUf9C0MJsRykyqz0_s_R1AlEY/'+image_url['result']['file_path'])
    image = cv2.imdecode(np.frombuffer(image.content, np.uint8), -1)
    return image

# Crop in square
def crop_square(image):
    square_side = 800
    # Cek panjang
    if image.shape[1] > image.shape[0]:
        longer_dim = 1
        long = image.shape[1]
        short = image.shape[0]
        other_shape = int(square_side * long / short)
        # yang lebih kecil jadikan 800
        new_shape = (square_side, other_shape)
    elif image.shape[0] > image.shape[1]:
        longer_dim = 0
        long = image.shape[0]
        short = image.shape[1]
        other_shape = int(square_side * long / short)
        # yang lebih kecil jadikan 800
        new_shape = (other_shape, square_side)
    # resize image
    image = cv2.resize(image, new_shape[::-1]) # opencv use reversed indexing
    # make it rectangle
    # Finding the cut point
    cut_point = int((other_shape - square_side) / 2 )
    # crop the image
    if longer_dim == 1:
        cropped_image = image[:, cut_point:cut_point+square_side]
    else:
        cropped_image = image[cut_point : cut_point+square_side, :]
    return cropped_image

@app.route('/', methods = ['POST', 'GET'])
def home():
    if request.method == 'POST':
        message = request.get_json()
        chat_id = message['message']['chat']['id']
        if 'text' in message['message']:
            text = message['message']['text']
        else:
            text = 'photo'

        # user start first conversation with bot
        # add user to session
        # initialize state to 0
        if chat_id not in session['chat_id']:
            session['chat_id'].append(chat_id)
            session['state'].append(0)
            session['photos'].append([])

        # get the user id
        id = session['chat_id'].index(chat_id)

        # user start by command /start
        if text == '/start':
            # initialize state to 1
            session['state'][id] = 1
            # logger
            requests.post(root + path, json={'chat_id': '@logger_gridol', 'text': str(message)})
            reply = "We will help you grid your photos. \nPlease upload your first image..."
            requests.post(root + path, json={'chat_id': chat_id, 'text': reply})
        elif text == '/log':
            # check session variable
            requests.post(root + path, json={'chat_id': '@logger_gridol', 'text': str(session)})
        elif text == 'Finish':
            if session['state'][id] == 0:
                # Invalid user request to process the image to grid
                # state is 0. tell user to start first
                reply = 'please start a request first using /start command'
                requests.post(root + path, json={'chat_id': chat_id, 'text': reply})
            else:
                # Valid user request to process the image to grid
                images = [] # array of requested image
                download_url = 'https://api.telegram.org/bot851968246:AAGT9gO713AUf9C0MJsRykyqz0_s_R1AlEY/getFile?file_id='
                upload_url = 'https://api.telegram.org/bot851968246:AAGT9gO713AUf9C0MJsRykyqz0_s_R1AlEY/sendPhoto?chat_id='
                # Download photos then send user the processed grid photo
                # 1. Download the photos
                for photo_url in session['photos'][id]:
                    image = download_photo(download_url + photo_url)
                    images.append(image)
                # 1.5 Process photo to grid
                # Get the row first. round to top so
                row = int(np.ceil(len(images)/2))
                # Create an empty numpy image container
                container = np.zeros((row * height, 2 * width, 3))
                # Loop through each row, inserting all the images to each cell
                for i in range(row):
                    try:
                        # loop through the image when processed image is not even
                        left_photo = crop_square(images[i * 2])
                        right_photo = crop_square(images[i * 2 + 1])
                        # Insert left image to the container
                        container[i*height:(i+1)*height, 0:width] = left_photo
                        # Insert right image to the container
                        container[i*height:(i+1)*height, width:2*width] = right_photo
                    except Exception as e:
                        #  Add last image when requesting even images
                        photo = cv2.resize(images[i * 2], (2*width, height))
                        container[i*height:(i+1)*height, 0:2*width] = photo
                # 2. Save photo to temporary directory
                cv2.imwrite('./temp.jpg', container)
                requests.post(root + path, json={'chat_id': '@logger_gridol', 'text': str(len(images))})
                # 3. Send photo back to user
                # json vs data
                # https://2.python-requests.org/en/master/user/quickstart/#more-complicated-post-requests
                image = cv2.imread('./temp.jpg')

                files = {'photo': open('./temp.jpg', 'rb')}
                try:
                    # Uploading photos to the user
                    r = requests.post(upload_url + str(chat_id), files=files)
                    # Mark request as finish
                    session['state'][id] = 0
                    session['photos'][id] = []
                    # Reply message to user
                    reply = 'Grid complete!!\nYou may start again by tapping start'
                    reply_markup = reply_markup={"keyboard":[["/start"]],"one_time_keyboard":True}
                    requests.post(root + path, json={'chat_id': chat_id, 'text': reply, 'reply_markup':reply_markup})
                except Exception as e:
                    requests.post(root + path, json={'chat_id': '@logger_gridol', 'text': str(e)})
        elif text == 'Cancel':
            # Clear the session for this user
            session['state'][id] = 0
            session['photos'][id] = []
            # Reply to the user
            reply = 'Request Canceled\nYou may start again by tapping start'
            reply_markup = reply_markup={"keyboard":[["/start"]],"one_time_keyboard":True}
            requests.post(root + path, json={'chat_id': chat_id, 'text': reply, 'reply_markup':reply_markup})
        elif 'photo' in message['message']:
            if session['state'][id] == 0:
                requests.post(root + path, json={'chat_id': '@logger_gridol', 'text': str(message)})
                reply = 'please start a request first using /start command'
                requests.post(root + path, json={'chat_id': chat_id, 'text': reply})
            else:
                # User has state 1
                session['photos'][id].append(message['message']['photo'][-1]['file_id'])

                photo_counter = len(session['photos'][id])

                reply = 'Photo added. ' + str(photo_counter) + ' photos ready.\nAdd more photos by sending next photos.\nTap Finish to process your grid, or Cancel to abandon your request'

                reply_markup={"keyboard":[["Finish"], ['Cancel']],"one_time_keyboard":True}

                requests.post(root + path, json={'chat_id': chat_id, 'text': reply, 'reply_markup': json.dumps(reply_markup)})

        else:
            requests.post(root + path, json={'chat_id': '@logger_gridol', 'text': str(message)})
            requests.post(root + path, json={'chat_id': chat_id, 'text': str(text)})

        return '', 200
    elif request.method == 'GET':
        message = request.args.get('message')
        return 'hello %s' % message

@app.route('/hello/<name>')
def hello_name(name):
    return 'hello %s' % name

@app.route('/test', methods = ['POST', 'GET'])
def test():
    if request.method == 'POST':
        message = request.form['message']
        return 'you submit = %s' % message
    elif request.method == 'GET':
        message = request.args.get('message')
        return 'message is %s' % message

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, ssl_context='adhoc')